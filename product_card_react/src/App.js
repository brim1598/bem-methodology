import "./style.scss";

const App = () => {
  return (
    <div className="product">
      <img
        src="./iphone_14_pro.png"
        alt="Iphone 14 Pro"
        className="product__image"
      />
      <div className="product__details">
        <h1 className="product__title mb">Iphone 14 Pro</h1>
        <div className="product__details__columns_box mb">
          <div className="product__price">
            <div className="product__price__label">Price:</div>
            <div className="product__price__price">$999.99</div>
          </div>
          <div className="product__available_in_stock">
            <div className="product__available_in_stock__label">
              Availability:
            </div>
            <div className="product__available_in_stock__status--success">
              In stock
            </div>
          </div>
        </div>
        <div className="product__description mb">
          The iPhone 14 Pro models are much more feature rich than the iPhone 14
          models, offering camera technology improvements, better display
          capabilities, a faster A16 chip, and more. The 6.1 and 6.7-inch iPhone
          14 Pro models look like the iPhone 13 Pro models with flat edges,
          stainless steel enclosure, IP68 water resistance, and a Ceramic
          Shield-protected display, but the camera bumps are larger to
          accommodate new lenses, and the display has also changed.
        </div>
        <div className="product__rating mb">
          <span className="product__rating__label">Rating:</span>
          <span className="product__rating__result--success">4.5 / 5</span>
        </div>
        <div className="product__details__columns_box">
          <button className="product__button--primary">Buy now</button>
          <button className="product__button--secondary">Add to cart</button>
        </div>
      </div>
    </div>
  );
};

export default App;
